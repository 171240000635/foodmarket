part of 'models.dart';

class Food extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final String ingredients;
  final int price;
  final double rate;

  Food(
      {this.id,
      this.picturePath,
      this.name,
      this.description,
      this.ingredients,
      this.price,
      this.rate});

  @override
  // TODO: implement props
  List<Object> get props =>
      [id, picturePath, description, ingredients, price, rate];
}

List<Food> mockFoods = [
  Food(
      id: 1,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 100000,
      rate: 4.2),
  Food(
      id: 2,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik Super",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 100000,
      rate: 4.2),
  Food(
      id: 3,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik Paket Lumayan Hemat",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 100000,
      rate: 4.2),
  Food(
      id: 1,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik Paket Super Boros",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 1000000,
      rate: 4.2),
  Food(
      id: 1,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 100000,
      rate: 4.2),
  Food(
      id: 1,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 100000,
      rate: 4.2),
  Food(
      id: 1,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 100000,
      rate: 4.2),
  Food(
      id: 1,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 100000,
      rate: 4.2),
  Food(
      id: 1,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 100000,
      rate: 4.2),
  Food(
      id: 1,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 100000,
      rate: 4.2),
  Food(
      id: 1,
      picturePath: "assets/food/food_ku.png",
      name: "Tahu Walik",
      description: "Tahu Walik Makanan Nyeleneh B aja",
      ingredients: "Tahu, Bakso, Dan Resep Rahasia",
      price: 100000,
      rate: 4.2),
];

