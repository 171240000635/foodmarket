part of 'models.dart';

class User extends Equatable {
  final int id;
  final String name;
  final String email;
  final String address;
  final String houseNumber;
  final String phoneNumber;
  final String city;
  final String picturePath;

  User(
      {this.id,
      this.name,
      this.email,
      this.address,
      this.houseNumber,
      this.phoneNumber,
      this.city,
      this.picturePath});
  @override
  // TODO: implement props
  List<Object> get props =>
      [id, name, email, address, houseNumber, phoneNumber, city, picturePath];
}

User mockUser = User(
    id: 1,
    name: 'Reza Ramadhan',
    address: 'Donorojo',
    city: 'Jepara',
    houseNumber: '1234',
    phoneNumber: '0812598818',
    email: '171240000635@unisnu.ac.id',
    picturePath:
        'assets/avatar.png');

